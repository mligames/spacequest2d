﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject, ICharacterController
{
    public Camera Camera;
    private SpriteRenderer spriteRenderer;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.ForceSoftware;
    private Vector2 hotSpot;
    public Vector2 MousePosition { get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); } }

    [SerializeField] private Collider2D characterCollider;
    public Collider2D CharacterCollider { get => characterCollider; set => characterCollider = value; }

    [SerializeField] private WeaponController weaponController;
    public WeaponController WeaponController { get => weaponController; set => weaponController = value; }
    public WeaponData StartingWeapon;

    public float MoveSpeed;
    private void Awake()
    {
        hotSpot = new Vector2(cursorTexture.width / 2, cursorTexture.height / 2);
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        characterCollider = GetComponent<Collider2D>();
        if (Camera == null)
        {
            Camera = Camera.main;
            Camera.transform.parent = this.transform;
        }
        if(weaponController == null)
        {
            WeaponController = GetComponentInChildren<WeaponController>();
        }
    }
    private void Start()
    {
        EquipWeapon(StartingWeapon);
    }
    protected override void Update()
    {
        base.Update();
        WeaponController.RotateWeapon(LookDirection());
        if (Input.GetMouseButtonDown(0))
        {
            FireWeapon();
        }
    }
    public Vector2 LookDirection() 
    {
        return (MousePosition - (Vector2)transform.position).normalized;
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;
        Vector2 look = LookDirection();
        move.x = Input.GetAxis("Horizontal");
        move.y = Input.GetAxis("Vertical");
        FlipPlayerSprite(look);
        //WeaponController.FlipWeaponSprite(look);
        targetVelocity = move * MoveSpeed;

    }
    private void FlipPlayerSprite(Vector2 look)
    {
        if (look.x > 0.01f)
        {
            if (spriteRenderer.flipX == true)
            {
                spriteRenderer.flipX = false;
            }
        }
        else if (look.x < -0.01f)
        {
            if (spriteRenderer.flipX == false)
            {
                spriteRenderer.flipX = true;
            }
        }
    }
    public void EquipWeapon(WeaponData weaponData)
    {
        weaponData.EquipWeapon(WeaponController);
    }
    public void FireWeapon()
    {
        WeaponController.EquippedWeaponData.Fire(this);
    }

    public Vector2 CharacterPosition()
    {
        return transform.position;
    }

    public Collider2D OwnerCollider()
    {
        return characterCollider;
    }

    public string OwnerName()
    {
        return gameObject.name;
    }

    public WeaponController GetWeapon()
    {
        return weaponController;
    }
    private void OnDrawGizmos()
    {
        Vector2 pos = Input.mousePosition;
        Gizmos.color = Color.yellow;
        Vector2 stw = Camera.main.ScreenToWorldPoint(pos);
        Gizmos.DrawLine(transform.position, stw);
    }
}
