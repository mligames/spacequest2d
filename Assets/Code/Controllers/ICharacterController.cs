﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacterController
{
    string OwnerName();
    Vector2 CharacterPosition();
    Collider2D CharacterCollider { get; set; }
    WeaponController WeaponController { get; set; }
    void EquipWeapon(WeaponData weaponData);
    void FireWeapon();

}