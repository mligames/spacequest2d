﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAim : MonoBehaviour
{
    public PlayerController playerController;
    public Camera aimCamera;
    public float aimCameraSpeed;
    public float aimLeashDistance;

    public LineRenderer aimLine;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public Vector2 MousePosition { get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); } }


    private void Awake()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        aimCamera = Camera.main;
        if (aimLine == null)
        {
            aimLine = GetComponent<LineRenderer>();
        }
        aimLine.SetPosition(0, playerController.transform.position);
    }

    public void MoveAimCamera()
    {
        var heading = MousePosition - (Vector2)transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance; // This is now the normalized direction.

        //If Outside Leach Distance
        if (heading.sqrMagnitude > aimLeashDistance * aimLeashDistance)
        {
            Vector3 velocity = (direction * aimLeashDistance) - (Vector2)aimCamera.transform.position;
            Vector3 pos = Vector3.SmoothDamp(aimCamera.transform.position, MousePosition, ref velocity, 0.3f, aimCameraSpeed);
            pos.z = -1.0f;
            aimCamera.transform.position = pos;
            aimLine.SetPosition(0, playerController.transform.position);
            aimLine.SetPosition(1, pos);
        }
        else //If inside Leash Distance
        {
            Vector3 velocity = (Vector2)transform.position - (Vector2)aimCamera.transform.position;
            Vector3 pos = Vector3.SmoothDamp((Vector2)aimCamera.transform.position, transform.position, ref velocity, 0.1f, aimCameraSpeed);
            pos.z = -1.0f;
            aimCamera.transform.position = pos;
            aimLine.SetPosition(0, playerController.transform.position);
            aimLine.SetPosition(1, pos);
        }
    }
    private void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.yellow;
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, aimLeashDistance);
    }
}
