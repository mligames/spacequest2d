﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMouseInput : MonoBehaviour
{
    [Serializable]
    public class MoveAxis
    {
        public KeyCode Positive;
        public KeyCode Negative;

        public MoveAxis(KeyCode positive, KeyCode negative)
        {
            Positive = positive;
            Negative = negative;
        }

        public static implicit operator float(MoveAxis axis)
        {
            return (Input.GetKey(axis.Positive)
                ? 1.0f : 0.0f) -
                (Input.GetKey(axis.Negative)
                ? 1.0f : 0.0f);
        }
    }

    public PlayerController PlayerController;

    public float MoveRate;
    public MoveAxis Horizontal = new MoveAxis(KeyCode.D, KeyCode.A);
    public MoveAxis Vertical = new MoveAxis(KeyCode.W, KeyCode.S);

    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public Vector2 MousePosition { get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); } }

    private void Awake()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    public Vector3 MovementDirection()
    {
        Vector3 moveNormal = new Vector3(Horizontal, Vertical, 0.0f).normalized;
        return moveNormal * Time.deltaTime * MoveRate;
    }
    public Vector3 LookDirection()
    {
        return (MousePosition - (Vector2)PlayerController.transform.position).normalized;
    }
}
