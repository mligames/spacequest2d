﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class ProjectileMunition : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Collider2D projectileCollider;
    public ICharacterController Owner;
    public float Speed;
    public float LifeTime;
    public float Range;
    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        projectileCollider = GetComponent<Collider2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        rb2d.velocity = transform.right * Speed;
    }
    public void SetOwner(ICharacterController owner)
    {
        Owner = owner;
        Debug.Log(Owner.OwnerName() + " fired " + this.name);
        Physics2D.IgnoreCollision(Owner.CharacterCollider, projectileCollider);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Hit: " + collision.gameObject.name);
        Destroy(gameObject);
    }
}
