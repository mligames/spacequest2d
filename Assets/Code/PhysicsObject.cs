﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    protected Rigidbody2D rb2d;
    public float gravityModifier = 0f;
    protected Vector2 targetVelocity;
    protected Vector2 velocity;
    protected const float minMoveDistance = 0.001f;

    void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.gravityScale = gravityModifier;
    }

    protected virtual void Update()
    {
        targetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {

    }

    void FixedUpdate()
    {
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        velocity = targetVelocity;

        Vector2 deltaPosition = velocity * Time.deltaTime;
        Movement(deltaPosition);
    }

    void Movement(Vector2 move)
    {
        float distance = move.magnitude;
        rb2d.position += move.normalized * distance;
    }
}
