﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public WeaponData EquippedWeaponData;
    private SpriteRenderer weaponSpriteRenderer;

    public Transform FirePoint;
    public Collider2D OwnerCollider;

    public Vector2 MousePosition { get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); } }
    void Awake()
    {
        if(weaponSpriteRenderer == null)
        {
            weaponSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }
        if(OwnerCollider == null)
        {
            OwnerCollider = GetComponentInParent<Collider2D>();
        }

    }

    public void ChangeWeaponSprite(Sprite sprite)
    {
        weaponSpriteRenderer.sprite = sprite;
    }
    public void RotateWeapon(Vector2 look)
    {
        Vector2 heading = MousePosition - (Vector2)transform.position;
        var angle = Mathf.Atan2(heading.y, heading.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, angle);

        if (look.x > 0.01f)
        {
            if (weaponSpriteRenderer.flipX == true)
            {
                weaponSpriteRenderer.flipX = false;
            }
            if (weaponSpriteRenderer.flipY == true)
            {
                weaponSpriteRenderer.flipY = false;
            }
        }
        else if (look.x < -0.01f)
        {
            if (weaponSpriteRenderer.flipY == false)
            {
                weaponSpriteRenderer.flipY = true;
            }
        }
    }
    public void FlipWeaponSprite(Vector2 look)
    {
        if (look.x > 0.01f)
        {
            if (weaponSpriteRenderer.flipX == true)
            {
                weaponSpriteRenderer.flipX = false;
            }
        }
        else if (look.x < -0.01f)
        {
            if (weaponSpriteRenderer.flipX == false)
            {
                weaponSpriteRenderer.flipX = true;
            }
        }
    }
}
