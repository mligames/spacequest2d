﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class WeaponData : ScriptableObject
{
    public abstract string DisplayName { get; set; }
    public abstract Sprite WeaponSprite { get; set; }
    public abstract void EquipWeapon(WeaponController WeaponController);
    public abstract void Fire(ICharacterController owner);
}
