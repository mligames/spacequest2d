﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newProjectileWeapon", menuName = "Weapons/Projectile")]
public class ProjectileWeaponData : WeaponData
{
    [SerializeField]
    private string displayName;
    public override string DisplayName { get => displayName; set => displayName = value; }

    [SerializeField]
    private Sprite weaponSprite;
    public override Sprite WeaponSprite { get => weaponSprite; set => weaponSprite = value; }

    [SerializeField]
    private ProjectileMunition munitionPrefab;
    public ProjectileMunition MunitionPrefab { get => munitionPrefab; set => munitionPrefab = value; }
    
    public override void EquipWeapon(WeaponController weaponController)
    {
        weaponController.EquippedWeaponData = this;
        weaponController.ChangeWeaponSprite(weaponSprite);
    }
    public override void Fire(ICharacterController owner)
    {
        ProjectileMunition munition;
        munition = GameObject.Instantiate(munitionPrefab, owner.WeaponController.FirePoint.transform.position, owner.WeaponController.FirePoint.transform.rotation);
        munition.transform.SetParent(null);
        munition.SetOwner(owner);
    }
}
